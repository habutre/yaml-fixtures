Java Yaml Fixtures in Practice
========================

> *Note:* This project was only possible due the excellent [snakeYaml](https://bitbucket.org/asomov/snakeyaml/src) project which is the golden core of these explained examples on how to create good fixtures without using lots of `new Object()` or `Object.builder()` everywhere.


This project just shows an example on how to use [snakeYaml](https://bitbucket.org/asomov/snakeyaml/src) to make your life easy when you think about your unit and integration tests using JUnit or the test framework of your preference.

Here I just show examples using the bare JUnit once the target is to provide a readable and easy way to create *Test Data* which reflects the business needs  in your tests.

Feel free to comment, use the examples and you are very welcome to comment out how are/were your experience and also how do you do this differently.

My motivation was not found easily a way to produce fixtures using a descriptive file as I did before using **RSpec** on *Ruby* world.

#### **Follow me**
![twitter](https://png.icons8.com/?id=13963&size=50) [@habutre](https://twitter.com/habutre)
![gitlab](https://png.icons8.com/?id=34886&size=50) [@habutre](https://gitlab.com/habutre)
![github](https://png.icons8.com/?id=16318&size=50) [@habutre](https://github.com/habutre)
![bitbucket](https://png.icons8.com/?id=32874&size=50) [@habutre](https://bitbucket.org/habutre/)
![linkedin](https://png.icons8.com/?id=13930&size=50) [Rogério Ramos da Silva](https://www.linkedin.com/in/rogerioramossilva)

####### icons loaded from ![icons8](https://png.icons8.com/?id=20825&size=50)[https://icons8.com](https://icons8.com/)

