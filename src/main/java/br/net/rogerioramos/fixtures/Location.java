package br.net.rogerioramos.fixtures;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Location {

    private Double latitude;

    private Double longitude;

    Location(){}
}


