package br.net.rogerioramos.fixtures;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class WayPoint {

    private int jobId;

    private WayPointType wayPointType;

    private Coordinate coordinate;

    WayPoint(){}
}
