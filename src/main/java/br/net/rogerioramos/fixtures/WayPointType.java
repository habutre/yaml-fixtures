package br.net.rogerioramos.fixtures;

public enum WayPointType {
    PICKUP,
    DELIVER
}
