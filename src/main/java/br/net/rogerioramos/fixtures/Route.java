package br.net.rogerioramos.fixtures;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
public class Route {

    private int id;

    private String reference;

    private int driverId;

    private Boolean enabled;

    private List<WayPoint> wayPoints;


    Route() {}
}
