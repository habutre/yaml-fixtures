package br.net.rogerioramos.fixtures;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DisplayName("Given a File")
class FixtureTest {

    @Nested
    @DisplayName("When the File exists and there is multiple entries")
    class PluralFilename {

        @Test
        @DisplayName("Then a map of objects is built")
        void loadMapOfObjects() {
            Map<String, Coordinate> mapOfObjects = Fixture.loadFixtures("coordinates.yaml");

            assertThat(mapOfObjects).hasSize(4);
            assertThat(mapOfObjects).containsKeys("anhangabau");

            Coordinate anhangabau = mapOfObjects.get("anhangabau");

            assertThat(anhangabau.getLatitude()).isEqualTo(-23.545418);
            assertThat(anhangabau.getLongitude()).isEqualTo(-46.636528);
        }
    }

    @Nested
    @DisplayName("When the File exists and there is a single object")
    class SingularFilename {

        @Test
        @DisplayName("Then a object is built")
        void loadObject() {
            Route object = Fixture.loadFixture("atPracaDaSeFromAvenidaPaulistaToButanta.yml", Route.class);

            assertThat(object.getId()).isEqualTo(1001);
            assertThat(object.getDriverId()).isEqualTo(100189);
        }
    }

    @Nested
    @DisplayName("When the File NOT exists")
    class NonExistentFile {

        @Test
        @DisplayName("Then an exception is threw")
        void loadFailure() {

            assertThatThrownBy(

                    () -> Fixture.loadFixture("inexistentFile.yml", Object.class)

            ).hasMessage("Fixture file not found. Ensure to place the file under << resources >> folder");
        }
    }

    @Nested
    @DisplayName("When the File exists, there is multiple object types on it")
    class testTypedObjects {

        @Test
        @DisplayName("Then the expected type should be casted")
        void loadFailure() {

            Map<String, ?> coordinates = Fixture.loadFixtures("coordinates.yaml");
            Location penha = (Location) coordinates.get("penha");
            coordinates = null; // discard unneeded values

            assertThat(penha.getLatitude()).isEqualTo(-23.533961);
        }
    }
}
