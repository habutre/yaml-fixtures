package br.net.rogerioramos.fixtures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class RouteTest {
    private static Route route;

    @BeforeAll
    static void loadFixtures() {
        route = Fixture.loadFixture("atPracaDaSeFromAvenidaPaulistaToButanta.yml", Route.class);
    }

    @Test
    @DisplayName("Example for generating a fixture yaml file")
    void dumpComplexType() {
        Coordinate pickupCoordinate = Coordinate.builder().latitude(41.393883d).longitude(13.85757d).build();
        Coordinate deliverCoordinate = Coordinate.builder().latitude(41.36567d).longitude(13.87578d).build();

        WayPoint pickUp = WayPoint.builder().jobId(3).wayPointType(WayPointType.PICKUP).coordinate(pickupCoordinate).build();
        WayPoint deliver = WayPoint.builder().jobId(4).wayPointType(WayPointType.DELIVER).coordinate(deliverCoordinate).build();

        Route route = Route.builder().id(1).reference("Xhyl99sdf").driverId(2).wayPoints(Arrays.asList(pickUp, deliver)).enabled(true).build();

        Yaml yaml = new Yaml();
        String dump = yaml.dump(route);

        assertThat(dump).isNotBlank();
        assertThat(dump).containsOnlyOnce("id: 1");
        assertThat(dump).containsOnlyOnce("reference: Xhyl99sdf");
        assertThat(dump).containsOnlyOnce("driverId: 2");
        assertThat(dump).containsOnlyOnce("wayPointType: PICKUP");
        assertThat(dump).containsOnlyOnce("latitude: 41.36567");
        assertThat(dump).containsOnlyOnce("longitude: 13.87578");
        assertThat(dump).containsOnlyOnce("wayPointType: DELIVER");
        assertThat(dump).containsOnlyOnce("latitude: 41.393883");
        assertThat(dump).containsOnlyOnce("longitude: 13.85757");
        assertThat(dump).containsOnlyOnce("enabled: true");
    }

    @Test
    @DisplayName("Example for loading a fixture yaml file")
    void loadComplexType() throws IOException {
        assertThat(route).isNotNull();

        assertThat(route.getId()).isEqualTo(1001);
        assertThat(route.getReference()).isEqualTo("Xhyl99sdf");
        assertThat(route.getDriverId()).isEqualTo(100189);
        assertThat(route.getEnabled()).isFalse();

        assertThat(route.getWayPoints()).hasSize(2);

        assertThat(route.getWayPoints().get(0).getJobId()).isEqualTo(103);
        assertThat(route.getWayPoints().get(0).getWayPointType()).isEqualTo(WayPointType.PICKUP);
        assertThat(route.getWayPoints().get(0).getCoordinate().getLatitude()).isEqualTo(41.393883);
        assertThat(route.getWayPoints().get(0).getCoordinate().getLongitude()).isEqualTo(13.85757);

        assertThat(route.getWayPoints().get(1).getJobId()).isEqualTo(104);
        assertThat(route.getWayPoints().get(1).getWayPointType()).isEqualTo(WayPointType.DELIVER);
        assertThat(route.getWayPoints().get(1).getCoordinate().getLatitude()).isEqualTo(41.36567);
        assertThat(route.getWayPoints().get(1).getCoordinate().getLongitude()).isEqualTo(13.87578);
    }
}