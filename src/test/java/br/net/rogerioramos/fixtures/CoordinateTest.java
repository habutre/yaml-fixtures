package br.net.rogerioramos.fixtures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CoordinateTest {
    private Map<String, ?> coordinates;

    @BeforeAll
    void setUp() {
        coordinates = Fixture.loadFixtures("coordinates.yaml");

        assertThat(coordinates).isNotNull();
        assertThat(coordinates.size()).isEqualTo(4);
        assertThat(coordinates).containsOnlyKeys("pracaDaSe", "penha", "anhangabau", "fecap");
    }

    @Test
    void loadCoordinatesForPracaDaSe() throws IOException {
        Coordinate pracaDaSe = (Coordinate) coordinates.get("pracaDaSe");

        assertThat(pracaDaSe).isInstanceOf(Coordinate.class);
        assertThat(pracaDaSe.getLatitude()).isEqualTo(-23.549723);
        assertThat(pracaDaSe.getLongitude()).isEqualTo(-46.633902);
    }

    @Test
    void loadCoordinatesForFecap() throws IOException {
        Coordinate fecap = (Coordinate) coordinates.get("fecap");

        assertThat(fecap).isInstanceOf(Coordinate.class);
        assertThat(fecap.getLatitude()).isEqualTo(-23.549723);
        assertThat(fecap.getLongitude()).isEqualTo(-46.636498);
    }

    @Test
    void loadCoordinatesForAnhangabau() throws IOException {
        Coordinate anhangabau = (Coordinate) coordinates.get("anhangabau");

        assertThat(anhangabau).isInstanceOf(Coordinate.class);
        assertThat(anhangabau.getLatitude()).isEqualTo(-23.545418);
        assertThat(anhangabau.getLongitude()).isEqualTo(-46.636528);
    }

    @Test
    void loadLocationForPenha() throws IOException {
        Location penha = (Location) coordinates.get("penha");

        assertThat(penha).isInstanceOf(Location.class);
        assertThat(penha.getLatitude()).isEqualTo(-23.533961);
        assertThat(penha.getLongitude()).isEqualTo(-46.542577);
    }
}