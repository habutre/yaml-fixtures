package br.net.rogerioramos.fixtures;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Map;

class Fixture {

    static <T> T loadFixture(String filename, Class<T> clazz) {
        Yaml loader = new Yaml();
        InputStream fileStream = findYamlFile(filename);

        return loader.loadAs(fileStream, clazz);
    }

    static <T> Map<String, T> loadFixtures(String filename) {
        Yaml loader = new Yaml();
        InputStream fileStream = findYamlFile(filename);

        return loader.load(fileStream);
    }

    private static InputStream findYamlFile(String filename){
        InputStream resourceAsStream = Fixture.class.getResourceAsStream("/".concat(filename));

        if (null == resourceAsStream)
            throw new RuntimeException("Fixture file not found. Ensure to place the file under << resources >> folder");

        return resourceAsStream;
    }
}
